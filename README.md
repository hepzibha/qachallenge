# Test Data Tool and Report Generator
A tool to create and insert employee and department test data and generate a report based on both the values. Below are the technologies used to build this tool.

* Scripting Language used - Java
* Test Automation Framework - Testng
* Build Automation Tool - Maven
* DB used - mySQL 

Other jars used:
* json-simple
* mysql-connector-java
* log4j
* testng

# Environment Setup
* Install Java
* Install maven
* For IDE - install Eclipse/InteliJ

# Global Variables
All the global variables utilised in the module is stored in src/test/java/resources/globalVariable.properties

# Test Data 
The test Data to be inserted into the table are placed in JSON format in src/test/java/resources/testData.json

# Test Execution
1. IDE:
    * Open src/java/testcases/TestSuite.java
    * Right-click, run as java/testng
    <br>(or)
    * Open testng.xml and run as testng
<br>
2. Maven:
    $ mvn test
   
#  Result Report of the tables
Steps to generate the report:
Step 1 : Enter the Department id (between 1-4) or/and Location in the below methods in src/test/java/testcases/TestSuite.java
<br>@Test
<br>public void validateTheDeptEmpData() throws SQLException {
<br>db.selectlocation("London");
<br>db.selectDeptID(2);
<br>db.selectDeptID(4);
<br>}

Step 2 : Save the changes and execute the test case
Step 3 : Report will be generated in 'Reports/ResultReport_<timestamp>.html' directory after the execution is completed.

# Debugging The Execution
The execution logs are stored in 'Reports/consoleoutput.log'

