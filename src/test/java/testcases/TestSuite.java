package testcases;

import utils.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.testng.*;
import org.testng.annotations.*;

class TestSuite {
	
  HTMLReportGenerator reptgen = new HTMLReportGenerator();
  DBUtils db = new DBUtils();
  
  @BeforeClass
  public void createTestData() throws SQLException{
  		db.tableCheck();
  }
  
  @Test
  public void validateTheDeptEmpData() throws SQLException {
	  db.selectlocation("London");
	  db.selectDeptID(2);  
	  db.selectDeptID(4);
  }
  
  @AfterClass
  public void teardown() throws SQLException{
	  	db.closeDBCon();
  }
}

