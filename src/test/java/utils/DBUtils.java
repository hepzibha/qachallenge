package utils;

import java.sql.*;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.*;
import java.io.*;

public class DBUtils{  

    public Connection con;
    public Statement stmt;
    JSONObject tdcontent;
    JSONParser parser = new JSONParser();
    ResultSet rs;
    HTMLReportGenerator reptgen = new HTMLReportGenerator();
    public String reportbody = ""; 

	public DBUtils(){
		try{  
			reptgen.logger = Logger.getLogger(DBUtils.class);
			Class.forName("com.mysql.cj.jdbc.Driver");  
			con=DriverManager.getConnection(reptgen.prop.getProperty("db.Address")+reptgen.prop.getProperty("db.dbName"),reptgen.prop.getProperty("db.userName"),reptgen.prop.getProperty("db.password"));
			stmt=con.createStatement();
			Object obj = parser.parse(new FileReader(reptgen.prop.getProperty("testdataJsonFile")));
            tdcontent =  (JSONObject) obj;

	}catch(Exception e){
		reptgen.logger.error(e.getMessage());
		}  
	}
  
  	public void tableCheck() throws SQLException{
  		try {
  				rs = stmt.executeQuery("Select count(DEPARTMENT_ID) from DEPARTMENTS");
  				while(rs.next()){
  					int count1 = rs.getInt(1);
  					if(count1 == 0){
  						insertDataIntoDept();
  					}else{
  						reptgen.logger.info("Department table with "+count1+ " rows present");
  					}              
  				}
  				rs = stmt.executeQuery("Select count(EMPLOYEE_ID) from EMPLOYEE");
  				while(rs.next()){
  					int count2 = rs.getInt(1);
  					if(count2 == 0){
  						insertDataIntoEmp();
  					}else{
  						reptgen.logger.info("employee table with "+count2+ " rows present");
  					}             
  				}
  		}catch (Exception e) {
  			String errMsg = e.getMessage();
  			if(errMsg.contains("Table 'mockemployeedb.DEPARTMENTS' doesn't exist")) {
  				createTables();
  				insertDataIntoDept();
  				insertDataIntoEmp();
  			}else {
  				e.printStackTrace();
  			}
  		}
    }
	
	public void createTables() throws SQLException {
		String createTable1 = "CREATE TABLE DEPARTMENTS (DEPARTMENT_ID INT(5) NOT NULL PRIMARY KEY, DEPARTMENT_NAME VARCHAR(50) NOT NULL, LOCATION VARCHAR(50) NOT NULL)"; 
		stmt.executeUpdate(createTable1);
		reptgen.logger.info("Created table DEPARTMENTS in given database."); 
		String createTable2 = "CREATE TABLE EMPLOYEE (EMPLOYEE_ID INT(10) NOT NULL PRIMARY KEY, EMPLOYEE_NAME VARCHAR(50) NOT NULL, JOB_TITLE VARCHAR(50) NOT NULL, MANAGER_ID INT(10), DATE_HIRED DATE, SALARY INT(10) NOT NULL, DEPARTMENT_ID INT(5) NOT NULL, CONSTRAINT fk_deptID FOREIGN KEY (DEPARTMENT_ID) REFERENCES DEPARTMENTS(DEPARTMENT_ID))"; 
		stmt.executeUpdate(createTable2);
		reptgen.logger.info("Created table EMPLOYEE in given database."); 
	}
    
    public void insertDataIntoDept() throws SQLException{
    	JSONArray deptArr = (JSONArray)tdcontent.get("DEPARTMENTS");
        for (int i = 0; i < deptArr.size(); i++) {
        	JSONObject deptLst = (JSONObject)deptArr.get(i);
            String insertData = "INSERT INTO DEPARTMENTS(`DEPARTMENT_ID`, `DEPARTMENT_NAME`, `LOCATION`) VALUES ("+deptLst.get("DEPARTMENT_ID")+",'"+deptLst.get("DEPARTMENT_NAME")+"','"+deptLst.get("LOCATION")+"')"; 
            stmt.executeUpdate(insertData);  
        }
        reptgen.logger.info(deptArr.size()+" rows inserted to DB");
    }
    
    public void insertDataIntoEmp() throws SQLException{
        JSONArray empArr = (JSONArray)tdcontent.get("EMPLOYEE");
        for (int i = 0; i < empArr.size(); i++) {
        	JSONObject empLst = (JSONObject)empArr.get(i);
            String insertData = "INSERT INTO EMPLOYEE(`EMPLOYEE_ID`, `EMPLOYEE_NAME`, `JOB_TITLE`, `MANAGER_ID`, `DATE_HIRED`, `SALARY`, `DEPARTMENT_ID`) VALUES ("+empLst.get("EMPLOYEE_ID")+",'"+empLst.get("EMPLOYEE_NAME")+"','"+empLst.get("JOB_TITLE")+"',"+empLst.get("MANAGER_ID")+",'"+empLst.get("DATE_HIRED")+"',"+empLst.get("SALARY")+","+empLst.get("DEPARTMENT_ID")+")"; 
            stmt.executeUpdate(insertData);  
        }
        reptgen.logger.info(empArr.size()+" rows inserted to DB");
    }
    
	public ResultSet selectDeptID(int deptId) throws SQLException {
		String selectTable = "SELECT EMPLOYEE_ID, EMPLOYEE_NAME, JOB_TITLE, SALARY FROM EMPLOYEE WHERE DEPARTMENT_ID = "+deptId; 
		rs = stmt.executeQuery(selectTable);
      	if (rs != null){
      		String appendedBody = reptgen.constructTheFileBodyForDept(deptId, rs);
      		reportbody = reportbody+appendedBody;
        }
        return rs;
	}
    
    public ResultSet selectlocation(String loc) throws SQLException {
		String selectTable = "SELECT EMPLOYEE.EMPLOYEE_ID,EMPLOYEE.EMPLOYEE_NAME,EMPLOYEE.JOB_TITLE,EMPLOYEE.SALARY,DEPARTMENTS.DEPARTMENT_NAME FROM EMPLOYEE JOIN DEPARTMENTS ON DEPARTMENTS.DEPARTMENT_ID = EMPLOYEE.DEPARTMENT_ID WHERE DEPARTMENTS.LOCATION ='"+loc+"'"; 
		rs = stmt.executeQuery(selectTable);
      	if (rs != null){
      		String appendedBody = reptgen.constructTheFileBodyForloc(loc, rs);
      		reportbody = reportbody+appendedBody;
        }
        return rs;
	}
    
    public void closeDBCon() throws SQLException{
        con.close();
        reptgen.logger.info("Closed all open connections");
        reptgen.createWriteHtmlReport(reportbody);
        
    }
}  