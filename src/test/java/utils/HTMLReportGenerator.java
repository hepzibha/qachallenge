package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.Scanner;
//import java.util.logging.Logger;

import org.apache.log4j.Logger;

import java.util.Date;
import java.util.Properties;
import java.text.SimpleDateFormat;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;

public class HTMLReportGenerator {
	public static Properties prop;
	static Logger logger;
 
	//= Logger.getLogger(HTMLReportGenerator.class);
	
	public HTMLReportGenerator() {
		logger = Logger.getLogger(HTMLReportGenerator.class);
		try(InputStream input = new FileInputStream("../qachallenge/src/test/resources/globalVariable.properties"))
		{
			 prop = new Properties();
			 prop.load(input);
		}catch(Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}
	
    
    public String constructTheFileBodyForDept(int selcriteria, ResultSet dbvalues) throws SQLException{
        String body = "";
    	body = body+"<h4 align=\"center\">RESULT BASED ON DEPARTMENT ID : "+selcriteria+"</h4><table align=\"center\" style=\"width:80%\"><tr><th>EMPLOYEE ID</th><th>EMPLOYEE NAME</th><th>JOB TITLE</th><th>SALARY</th></tr>";
        
        while(dbvalues.next()) {
        	body = body+"<tr><td>"+dbvalues.getInt("EMPLOYEE_ID")+"</td><td>"+dbvalues.getString("EMPLOYEE_NAME")+"</td><td>"+dbvalues.getString("JOB_TITLE")+"</td><td>"+dbvalues.getInt("SALARY")+"</td></tr>";
        } 
        body = body+"</table><br><br>";
        return body;
    }
        
    public String constructTheFileBodyForloc(String selcriteria, ResultSet dbvalues) throws SQLException{
    	String body = "";
    	body = body+"<h4 align=\"center\">RESULT BASED ON LOCATION : "+selcriteria+"</h4><table align=\"center\" style=\"width:80%\"><tr><th>EMPLOYEE ID</th><th>EMPLOYEE NAME</th><th>JOB TITLE</th><th>SALARY</th><th>DEPARTMENT NAME</th></tr>";
        
      while(dbvalues.next()) {
            body = body+"<tr><td>"+dbvalues.getInt("EMPLOYEE_ID")+"</td><td>"+dbvalues.getString("EMPLOYEE_NAME")+"</td><td>"+dbvalues.getString("JOB_TITLE")+"</td><td>"+dbvalues.getInt("SALARY")+"</td><td>"+dbvalues.getString("DEPARTMENT_NAME")+"</td></tr>";
    	}
    	body = body+"</table><br><br>"; 
    	return body;
	}
  
	public void createWriteHtmlReport(String content){
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_hhmmss");
        Date curDate = new Date();
        String strDate = sdf.format(curDate);
        String NewfileName = "../qachallenge/Reports/ResultReport_" + strDate + ".html";
        
        try {
            FileWriter newRWFile = new FileWriter(NewfileName);
            BufferedReader template = new BufferedReader(new FileReader(prop.getProperty("htmlReportTemplate")));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            String ls = System.getProperty("line.separator");
            while ((line = template.readLine()) != null) {
            	stringBuilder.append(line);
            	stringBuilder.append(ls);
            }
            String data = stringBuilder.toString();
      		
          	data = data.replaceAll("bodyContentReplace", content);
          	
          	logger.info("bodyContent: "+data);
            newRWFile.write(data);
            
           	newRWFile.close();
          	template.close();
          	logger.info("HTML Report created successfully");
        } catch (IOException e) {
        	logger.error("An error occurred while generating the html file");
            e.printStackTrace();
        }
    }  
}

